TARGET = dns
LIBS = -luv
CC = gcc 
CFLAGS = -g -Wall -pthread -ansi -std=gnu99

.PHONY: default all clean

default: $(TARGET)
all: default

OBJECTS = ezxml/ezxml.o main.o string_list.o dns.o config.o
HEADERS = main.c string_list.c dns.c config.c

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): $(OBJECTS)
	$(CC) $(OBJECTS) -Wall $(LIBS) -o $@ 

clean:
	-rm -f *.o
	-rm -f $(TARGET)
