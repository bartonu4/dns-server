#include "config.h"



void parse_config()
{
    ezxml_t config = ezxml_parse_file("config.xml"), address, server,black_list;
    Config.server_dns = strdup( ezxml_child(config, "server")->txt);
    Config.code = strdup(ezxml_child(config, "response")->txt);
    black_list = ezxml_child(config, "black-list");

    for (address = ezxml_child(black_list, "address"); address; address = address->next)

    {
        add_to_list(&Config.head, address->txt);
        debug_print("host %s\n",address->txt);

    }

    ezxml_free(config);

}
 void free_config()
{
    free_list(Config.head);
    free(Config.server_dns);
    free(Config.code);
}
