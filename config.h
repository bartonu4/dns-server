#ifndef CONFIG_H
#define CONFIG_H

#include "ezxml/ezxml.h"
#include "string_list.h"
#include "debug_utils.h"

typedef struct
{
    string_list *head;
    char *server_dns;
    char *code;
} CONFIG;
extern CONFIG Config;

void parse_config();
void free_config();



#endif
