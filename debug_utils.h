#ifndef DEBUG_H
#define DEBUG_H

#define DEBUG;
#ifdef DEBUG
#define DEBUG_TEST 1
#else
#define DEBUG_TEST 0
#endif

#define debug_uv(msg, code) do {    if (DEBUG_TEST)                                                      \
    fprintf(stderr, "%s: [%s: %s]\n", msg, uv_err_name((code)), uv_strerror((code)));   \
    assert(0);                                                                          \
    } while(0);

#define debug_print(fmt, ...) \
            do { if (DEBUG_TEST) \
            flockfile(stdout);\
            printf(fmt,##__VA_ARGS__); \
            funlockfile(stdout);\
                        } while (0);
#define debug_perror(fmt) \
            do { if (DEBUG_TEST) \
            perror(fmt); \
                        } while (0);



#endif
