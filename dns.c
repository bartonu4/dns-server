#include "dns.h"



int check_packet(const char *msg)
{
	HEADER  *header = (HEADER * ) msg;
    if(header->qr == QR_QUERY)
	{
		return 1;
	}
	else 
	{
		return 0;
	}
	
}

const char* parse_name(char *msg)
{
	unsigned int pos = HEADER_SIZE, offset = 0, name_pos = 0;
	int pointer = 0, label_cnt=msg[pos];
	char *name = malloc(MSG_SIZE);
	//increment pos in buffer to skip first label count 
	pos++;
	while (msg[pos] != 0)
	{
		
		if(msg[pos] > 192)
		{
			offset = (msg[pos]*256 + msg[++pos]) & (0x3FFF);
			pointer = 1; 
			pos = offset;
			label_cnt = msg[offset];
			
		}
		
		else if(label_cnt == 0)
		{
			name[name_pos++] = '.';
			label_cnt = msg[pos++];
		}
		else 
		{
			name[name_pos++] = msg[pos++];
			
			label_cnt--;
		}
		
		
	}
	if(pointer == 1 )
	{
		name_pos++;
	}
    debug_print("host %s\n", name);

	return name;
}

//return  1 if list contains host
int match_name(string_list * list, const char *host)
{
	
	string_list *iterator  = list;
	
	while(iterator)
	{
		if(strstr(host, iterator->string)!=NULL)
		{
			
			return 1;
		}
		else if ( iterator->next!=NULL)
		{
		
			iterator = iterator->next;
		}
		else 
		{
	
			return 0;
		}		
	}
}

const char* make_neg_response(int code, char *msg, ssize_t len)
{
   char *resp = malloc(len);
   memcpy(resp,msg,len);
   HEADER  *header = (HEADER * ) resp;
   header->rcode = (unsigned) code;
   header->qr = QR_RESP;
   return resp;

}
