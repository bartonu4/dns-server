#ifndef DNS_H
#define DNS_H
#include <sys/socket.h>
#include "string_list.h"
#include "debug_utils.h"
#include "uv.h"

// status codes 
#define RC_OK       0
#define RC_FORMAT   1
#define RC_SERVFAIL 2
#define RC_NAMEERR  3
#define RC_NOTSUPP  4
#define RC_REFUSED  5
#define RC_BADVERS 16

// query response 
#define QR_QUERY    0
#define QR_RESP     1

//opcodes 

#define OP_QUERY    0
#define OP_IQUERY   1
#define OP_STATUS   2

//header of dns packet size 
#define HEADER_SIZE 12
#define MSG_SIZE 256

typedef struct {
    unsigned short id;

    unsigned char rd :1; // recursion desired
    unsigned char tc :1; // truncated message
    unsigned char aa :1; // authoritive answer
    unsigned char opcode :4; // 0 standard query
    unsigned char qr :1; // 0 query, 1 response


    unsigned char rcode :4;  // response code
    unsigned char z :3; // reserved
    unsigned char ra :1; // recursion available






    unsigned q_count :16; // number of question entries
    unsigned ans_count :16; // number of answer entries
    unsigned add_count :16; // number of resource entries
    unsigned auth_count :16; // number of authority entries

} HEADER;

typedef struct
{
    uv_work_t *		req;
    uv_udp_t *		handle;
    char *			buf	;
    struct sockaddr addr;
    int 			len;



}  msg_worker;

int check_packet(const char *msg);
const char *parse_name(char *msg);
int match_name(string_list * list, const char *host);
const char *make_neg_response(int code, char *msg, ssize_t len);









#endif
