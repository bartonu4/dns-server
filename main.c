

#include <assert.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "config.h"
#include <uv.h>

#include "dns.h"
#define BUFSIZE 1024
#define SERVICE_PORT 53




uv_loop_t* loop;


CONFIG Config;


static void on_alloc(uv_handle_t* client, size_t suggested_size, uv_buf_t* buf) 
{
    suggested_size = BUFSIZE;
    buf->base = malloc(suggested_size);
    buf->len = suggested_size;

}

void send_cb(uv_udp_send_t *req, int status)
{

    debug_print("data sended\n");
}
void after_send(uv_work_t *req, int status)
{
    // free memmory when thread end work

    msg_worker * packet = (msg_worker*) req->data;
    free(packet->buf);
    free(packet->req);
    free(packet);
    debug_print("thread end work\n");

}


void send_to_dns_cb(uv_work_t *req)
{

    int fd;
    int recvlen;
    int r;


    //get packet from worker
    msg_worker * packet = (msg_worker *) req->data;
    //set send socket
    uv_udp_t send_sock;
    uv_udp_send_t send_req;
    struct sockaddr_in remote_dns;
    struct sockaddr_in myaddr;  //sockaddr from which we send

    //set buf
    uv_buf_t *buf = malloc(sizeof(uv_buf_t));
    buf->base = packet->buf;
    buf->len = packet->len;
    // initialize remote addr
    memset((char *)&remote_dns, 0, sizeof(remote_dns));

    remote_dns.sin_family = AF_INET;
    remote_dns.sin_port = htons(53);
    if (inet_aton(Config.server_dns, &remote_dns.sin_addr)==0) {
        debug_print("inet_aton() failed\n");

    }

     // initialize my addr to communicate with remote dns
    memset((char *)&myaddr, 0, sizeof(myaddr));

    myaddr.sin_family = AF_INET;
    myaddr.sin_port = htons(0);
    myaddr.sin_addr.s_addr = htonl(INADDR_ANY);




    if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        debug_perror("cannot create socket\n");

    }
    if (bind(fd, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0) {
        debug_perror("bind failed");

    }

    //checking host in black list
    if(check_packet(buf->base))
    {
        debug_print("packet is good\n");
    }
    else
    {
        debug_print("packet is bad\n");
    }

    /* check if host from query is in black list*/

    char  *hosts = parse_name(buf->base);
    r = match_name(Config.head,hosts);
    free(hosts);


    
    if(r == 1)
    {
        /*host is in black list
         * return response to user
         * with error code
        */
        uv_buf_t *buf_r = malloc(sizeof(uv_buf_t));
        buf_r->base = make_neg_response(atoi(Config.code),buf->base,buf->len);
        buf_r->len = buf->len;

        //send from the same 53 port
        uv_udp_send(&send_sock,packet->handle,buf_r,1,&packet->addr,send_cb);
        free(buf_r->base);
        free(buf_r);

    }
    else
    {
        /*host is in white list
         * send query to dns
         *
        */

        if(sendto(fd, buf->base, buf->len, 0, (struct sockaddr *)&remote_dns, sizeof(remote_dns)) == -1)
        {
            debug_perror("send to error");
        }
        else
        {
            debug_print("data is sent white\n");
        }

        // set timeout for waiting answer from remote dns

        struct timeval timeout;
        timeout.tv_sec = 10;
        timeout.tv_usec = 0;
        setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO,(char*)&timeout,sizeof(timeout));


        char rcvbuf[BUFSIZE];



        recvlen = recvfrom(fd, rcvbuf, BUFSIZE, 0, NULL,0);
        if (recvlen < 0)
        {
            debug_perror ("recieved from error \n");

            if(r == EAGAIN)
            {
                //timeout
                uv_buf_t *buf_r = malloc(sizeof( uv_buf_t ));
                buf_r->base = make_neg_response( RC_SERVFAIL , buf->base,buf->len);
                buf_r->len = buf->len;

                //send from the same 53 port
                uv_udp_send(&send_sock,packet->handle,buf_r,1,&packet->addr,send_cb);

                free(buf_r->base);
                free(buf_r);



            }
        }
        if (recvlen > 0)
        {
            debug_print ("recieved from dns %d \n", recvlen);

            uv_buf_t *buf_ans = malloc(sizeof(uv_buf_t));
            buf_ans->base = rcvbuf;
            buf_ans->len = recvlen;


            uv_udp_send(&send_req,packet->handle,buf_ans,1,&packet->addr,send_cb);

            free(buf_ans);



        }

        close(fd);
        free(buf);
    }




    
}

void recv_cb(uv_udp_t* handle, ssize_t nread, const uv_buf_t* rcvbuf, const struct sockaddr* addr, unsigned flags)
{
    if(nread > 0)
    {
        // create packet to pass another thread
        msg_worker *packet = (msg_worker*) malloc(sizeof(msg_worker));
        packet->req 	  = (uv_work_t *)malloc(sizeof(uv_work_t) );
        packet->req->data = (void *) packet;
        packet->buf 	  =  malloc(nread);
        packet->len       =  nread;
        packet->handle       =  handle;


        memcpy(packet->buf,rcvbuf->base,nread);
        memcpy(&packet->addr,addr,sizeof(addr));

        // give work to thread pool
        uv_queue_work(loop,packet->req,send_to_dns_cb,after_send);

    }
    //    if(addr!=NULL)
    //    {
    //        struct  sockaddr_in* addr_s = ( struct  sockaddr_in*) addr;
    //        debug_print("received from %s\n",inet_ntoa( addr_s->sin_addr));
    //    }

    free(rcvbuf->base);


}
int
main(int argc, char **argv)
{


    parse_config();
    struct sockaddr_in myaddr;	/* our address */


    int fd;				/* our socket */
    unsigned char buf[BUFSIZE];	/* receive buffer */



    /* create a UDP socket */

    if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        debug_perror("cannot create socket\n");
        return 0;
    }


    //Event loop

    loop = malloc(sizeof(uv_loop_t));

    uv_udp_t server;
    uv_loop_init(loop);

    int r=uv_udp_init(loop,&server);
    if(r<0)
    {
        debug_uv("error udp init",r);
    }
    r = uv_ip4_addr("0.0.0.0", SERVICE_PORT, &myaddr);
    if(r<0)
    {
        debug_uv("address error",r);
    }
    r= uv_udp_bind(&server, (const struct sockaddr*)&myaddr,0);
    if(r<0)
    {

        debug_uv("error bind",r);
    }
    printf("working\n");
    uv_udp_recv_start(&server, on_alloc, recv_cb);


    uv_run(loop, UV_RUN_DEFAULT);

    uv_loop_close(loop);
    free_config();
    free(loop);

    return 0;

}
