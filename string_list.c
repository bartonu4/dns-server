#include "string_list.h"



void add_to_list(string_list ** head, const char *host)
{		//allocating memory for this node and its string 
		string_list * list = malloc(sizeof(string_list));
		list->string = strdup(host);
		list->next = *head;
		*head = list;
}
void free_list(string_list *head)
{
    string_list * node = head;
    while(node!=NULL)
    {
        string_list * temp = node;
        node = node->next;
        free(temp);

    }

}

