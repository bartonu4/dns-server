#ifndef STRINGLIST_H 
#define STRINGLIST_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
typedef struct 
{
   char * string;
   struct string_list *next;
} string_list;
void add_to_list(string_list ** list, const char *host);
void free_list(string_list *head );


#endif
